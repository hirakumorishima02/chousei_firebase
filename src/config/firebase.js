import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyADwfj9D5h2sgu_VJ9s5C7Ix72X_LDYcOA",
  authDomain: "chousei-firebase-c2870.firebaseapp.com",
  databaseURL: "https://chousei-firebase-c2870.firebaseio.com",
  projectId: "chousei-firebase-c2870",
  storageBucket: "",
  messagingSenderId: "283183711233",
  appId: "1:283183711233:web:c32539c904e5f7f3d9ec00"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
